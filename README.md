# multissh

Connect to several SSH servers, and get a shell to all of them at the
same time.

## Installation

```
pip install git+https://gitlab.com/ralt/multissh.git
```

## Usage

```
usage: multissh [-h] server [server ...]

positional arguments:
  server      server to connect to

optional arguments:
  -h, --help  show this help message and exit
```

For example:

```
multissh server1.foo.com server2.foo.com server3.foo.com
```

Here is an example session:

```
local:~$ multissh server1 server2
[server1] Last login: Fri Aug 10 00:15:08 2018 from foo.bar
[server1]
[server2] Last login: Fri Aug 10 00:14:08 2018 from foo.bar
[server1] root@server1:~#
[server2]
[server2] root@server2:~#
whoami
[server1]
[server2]
[server1] root
[server1] root@server1:~#
[server2] root
[server2] root@server2:~#
exit
[server1]
[server1] logout
[server1] Connection to 1.1.1.1 closed.
[server2]
[server2] logout
[server2] Connection to 2.2.2.2 closed.
local:~$
```

## License

MIT. See the [LICENSE](LICENSE) file.
