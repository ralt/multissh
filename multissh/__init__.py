import argparse
import atexit
import errno
import fcntl
import os
import pty
import select
import subprocess
import sys
import termios
import tty

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("servers", type=str, metavar="server", nargs="+", help="server to connect to")

    args = parser.parse_args()
    servers = args.servers

    stdin_fileno = sys.stdin.fileno()

    old = termios.tcgetattr(stdin_fileno)
    tty.setcbreak(sys.stdin)
    atexit.register(lambda: termios.tcsetattr(stdin_fileno, termios.TCSADRAIN, old))

    processes = {}
    stdins = []

    epoll = select.epoll()

    pipe_matches = {}

    for server in servers:
        master, slave = pty.openpty()
        stdout_read_end, stdout_write_end = os.pipe()
        stderr_read_end, stderr_write_end = os.pipe()
        processes[server] = subprocess.Popen(
            ["ssh", "-t", server],
            stdin=slave,
            stdout=stdout_write_end,
            stderr=stderr_write_end,
        )
        stdins.append(os.fdopen(master, "wb"))

        fcntl.fcntl(stdout_read_end, fcntl.F_SETFL, os.O_NONBLOCK)
        fcntl.fcntl(stderr_read_end, fcntl.F_SETFL, os.O_NONBLOCK)
        epoll.register(stdout_read_end, select.EPOLLIN | select.EPOLLET)
        epoll.register(stderr_read_end, select.EPOLLIN | select.EPOLLET)

        pipe_matches[stdout_read_end] = {
            "server": server,
            "stdout": True,
        }
        pipe_matches[stderr_read_end] = {
            "server": server,
            "stdout": False,
        }

    fcntl.fcntl(sys.stdin, fcntl.F_SETFL, os.O_NONBLOCK)
    epoll.register(sys.stdin, select.EPOLLIN | select.EPOLLET)


    def read_fileno(fileno):
        buf = ""
        while True:
            try:
                data = os.read(fileno, 1024)
            except OSError as e:
                if e.errno != errno.EAGAIN:
                    raise
                break
            buf += data
        return buf

    last_stdin_char = None
    while True:
        events = epoll.poll(0.1)
        if not events:
            for server, process in processes.items():
                if process.poll() is not None:
                    del processes[server]
                    continue
                break
            else:
                sys.exit(0)
        for fileno, event in events:
            if fileno == stdin_fileno:
                data = read_fileno(fileno)
                last_stdin_char = data[-1]
                for remote_stdin in stdins:
                    os.write(remote_stdin.fileno(), data)

                sys.stdout.write(data)
                sys.stdout.flush()
                continue

            data = read_fileno(fileno)
            match = pipe_matches[fileno]
            if data == last_stdin_char and match["stdout"]:
                continue
            for line in data.splitlines():
                sys.stdout.write(
                    "{color}[{server}]{endcolor} {line}\n".format(
                        color="" if match["stdout"] else "\033[91m\033[1m",
                        server=match["server"],
                        endcolor="" if match["stdout"] else "\033[0m",
                        line=line,
                    ),
                )
